package edu.ib.endpoints.data.dtos;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class GameDto {
    @Id
    private Long id;

    private Boolean started;
    private LocalDateTime created;

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStarted() {
        return started;
    }

    public void setStarted(Boolean started) {
        this.started = started;
    }
}
