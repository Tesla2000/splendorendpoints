package edu.ib.endpoints.data.enums;

public enum Gem {
    RED,
    GREEN,
    BLUE,
    BROWN,
    WHITE,
    GOLD
}
