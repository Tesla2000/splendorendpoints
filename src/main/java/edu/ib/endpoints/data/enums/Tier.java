package edu.ib.endpoints.data.enums;

public enum Tier {
    FIRST,
    SECOND,
    THIRD,
    RESERVE
}
