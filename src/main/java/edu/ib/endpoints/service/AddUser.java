package edu.ib.endpoints.service;

import edu.ib.endpoints.data.dtos.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AddUser {
    private final UserService userService;

    @Autowired
    public AddUser(UserService userService) {
        this.userService = userService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void addUser(){
        userService.save(new UserDto(1L,"admin", "password"));
    }
}
